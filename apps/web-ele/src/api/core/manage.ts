import { requestClient } from '#/api/request';

export namespace DemoTableApi {
  export interface PageFetchParams {
    any: any;
    pagination: {
      descending: boolean;
      page: number;
      rowsPerPage: number;
      sortBy: string;
    };
  }
}

/**
 * 获取示例表格数据
 */
export async function getManageListApi(
  url: string,
  params: DemoTableApi.PageFetchParams,
) {
  return requestClient.post<DemoTableApi.PageFetchParams>(url, params);
}

/**
 * 获取搜索表单配置
 */
export async function getManageFormApi() {
  return requestClient.get('pz');
}

/**
 * 添加管理
 */
export async function addManageApi(url: string, data: any) {
  return requestClient.post(url, data);
}

/**
 * 更新管理
 */
export async function updateManageApi(url: string, data: any) {
  return requestClient.post(url, data);
}

/**
 * 删除管理
 */
export function removeManageApi(url: string, data: any) {
  return requestClient.post(url, data);
}
