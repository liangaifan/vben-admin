import { requestClient } from '#/api/request';

/**
 * 获取配置文件
 */
export async function getConfigureApi(params: string) {
  return requestClient.post<string>('/auth/configure', { route: params });
}

/**
 * 获取表格数据
 */
export async function getTableDataApi(url: string, params: any) {
  return requestClient.post<any>(url, params);
}
