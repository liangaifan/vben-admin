// /monitor/server/index

import type { RouteRecordStringComponent } from '@vben/types';

import { requestClient } from '#/api/request';

/**
 * 获取系统信息
 */
export async function getSystemInfoApi() {
  return requestClient.post<RouteRecordStringComponent[]>(
    '/auth/monitor/server/index',
  );
}
