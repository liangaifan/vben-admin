import type { VbenFormProps } from '#/adapter/form';
import type { VxeGridProps } from '#/adapter/vxe-table';

import { useVbenModal } from '@vben/common-ui';

import { useVbenVxeGrid } from '#/adapter/vxe-table';
import { getManageFormApi, getManageListApi } from '#/api';
import AddModal from '#/views/admins/manage/modal/addModal.vue';
import UpdateModal from '#/views/admins/manage/modal/updateModal.vue';

// 初始化添加模态框和模态框API
export const [BaseAddModal, BaseAddModalApi] = useVbenModal({
  connectedComponent: AddModal,
});

// 初始化更新模态框和模态框API
export const [BaseUpdateModal, BaseUpdateModalApi] = useVbenModal({
  connectedComponent: UpdateModal,
});

interface RowType {
  category: string;
  color: string;
  id: string;
  price: string;
  productName: string;
  releaseDate: string;
}

// 表单配置
const formOptions: VbenFormProps = {
  // 默认展开
  collapsed: false,
  schema: [],
  // 控制表单是否显示折叠按钮
  showCollapseButton: false,
  // 按下回车时是否提交表单
  // submitOnEnter: false,
};

export const gridOptions: VxeGridProps<RowType> = {
  id: 'manage-grid',
  columns: [],
  toolbarConfig: {
    enabled: true,
    refresh: true,
    custom: true,
    zoom: {
      escRestore: true,
    },
  },
  customConfig: {
    immediate: true,
    storage: true,
  },
  height: 'auto',
  keepSource: true,
  pagerConfig: {
    total: 0,
    currentPage: 1,
    pageSize: 10,
  },
  proxyConfig: {
    seq: true,
    ajax: {
      query: async ({ page }, formValues) => {
        return await getManageListApi('/auth/manage/list', {
          ...formValues,
          pagination: {
            sortBy: 'id',
            descending: true,
            page: page.currentPage,
            rowsPerPage: page.pageSize,
          },
        });
      },
    },
  },
};

// 初始化表格和表格API
export const [Grid, gridApi] = useVbenVxeGrid({ formOptions, gridOptions });

let formData: any = {};

getManageFormApi().then((res) => {
  formOptions.schema = res.search;
  gridApi.setGridOptions({
    columns: [
      { type: 'checkbox', width: 60 },
      { title: '序号', type: 'seq', width: 50 },
      ...(res.columns ?? []),
      { slots: { default: 'action' }, title: '操作' },
    ],
  });

  formData = res;
});

// 打开更新模态框的函数
export const openAddModal = () => {
  BaseAddModalApi.setData({
    addForm: formData.tools,
  });
  BaseAddModalApi.open();
  BaseAddModalApi.setState({ title: '添加管理' });
};

// 打开更新模态框的函数
export const openUpdateModal = (row: any) => {
  BaseUpdateModalApi.setData({
    row,
    updateForm: formData.columnOptions[0],
  });
  BaseUpdateModalApi.open();
  BaseUpdateModalApi.setState({ title: '更新管理' });
};
